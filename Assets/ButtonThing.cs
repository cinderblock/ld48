using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonThing : MonoBehaviour
{
    public int offsetX = 0, offsetY = 5;
    Vector3 pos;

    void Start()
    {
        //pos = textRect.localPosition;
    }

    public void Down(GameObject thisButton)
    {
        
        pos = thisButton.gameObject.transform.position;
        thisButton.gameObject.transform.position = new Vector3(pos.x, pos.y - (float)offsetY, pos.z);
    }

    public void Up(GameObject thisButton)
    {
        pos = thisButton.gameObject.transform.position;
        thisButton.gameObject.transform.position = new Vector3(pos.x, pos.y + (float)offsetY, pos.z);

    }
}
