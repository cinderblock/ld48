using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public bool doShake = false;
    public float shakeDuration = 0.3f;
    private float originalShakeDur;
    public float shakeAmount = 0.3f;
    private float originalShakeAmt;
    Vector3 originalPos;
    public float decreaseFactor = 1f;
    // Start is called before the first frame update
    void Start()
    {
        originalPos = transform.position;
        originalShakeDur = shakeDuration;
        originalShakeAmt = shakeAmount;
        GameManager.instance.onBadCollission.AddListener(DoCameraShake);
    }

    // Update is called once per frame
    void Update()
    {
        if (shakeDuration > 0f && doShake)
        {
            transform.position = originalPos + Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime * decreaseFactor;
            shakeAmount -= Time.deltaTime * decreaseFactor;
        }
        else
            transform.position = originalPos;
    }

    public void DoCameraShake()
    {
        doShake = true;
        shakeDuration = 0.3f;
        shakeAmount = 0.3f;
    }
}
