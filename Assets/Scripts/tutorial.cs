using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorial : MonoBehaviour
{
    public Paralax paralax;
    // Start is called before the first frame update
    void Start()
    {
        paralax = gameObject.GetComponent<Paralax>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.hasGameStarted)
            //set this paralax to 5
            paralax.paralaxStrength = 5;
    }
}
