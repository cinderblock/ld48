using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class SliderVolumeType : MonoBehaviour
{
    public SoundType soundType;
    // Start is called before the first frame update
    void Start()
    {
        var am = AudioManager.instance.audioMixer;
        float vol = 0;
        if (soundType == SoundType.SFX)
            am.GetFloat("SFXVol", out vol);
        else if (soundType == SoundType.Ambiance)
            am.GetFloat("AmbienceVol", out vol);
        else if (soundType == SoundType.Music)
            am.GetFloat("MusicVol", out vol);
        else if (soundType == SoundType.None)
            am.GetFloat("MasterVol", out vol);
        vol = vol.DecibelToLinear();
        Debug.Log(vol);
        vol = Mathf.Clamp(vol, 0f, 1f);
        gameObject.GetComponent<Slider>().value = vol;
    }
}