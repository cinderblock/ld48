using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utilities;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public static GameManager instance;
    [HideInInspector]
    public bool isPaused = false;

    public GameObject playerOne;
    public GameObject playerTwo;
    public CharController controllerOne;
    public CharController controllerTwo;
    public GameObject offender;

    [SerializeField]
    private AnimationCurve difficultyRamp;
    private float diffRampDeltaTime = 0;

    public float paralaxMod = 1;
    public float minSpeed = 1f;
    public float maxSpeed = 3f;

    public bool gameOver = false;
    public bool hasGameStarted = false;

    public float bestDistance = 0f;
    public float curDistance = 0f;

    public GameObject gof;
    public GameObject flag;

    public int hearts = 3;

    //gameover event(s)?
    public UnityEvent onGameOver;
    public UnityEvent onBadCollission;

    public TMPro.TextMeshProUGUI tmpCurDistance;


    private void Awake()
    {
        //DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        controllerOne = playerOne.GetComponent<CharController>();
        controllerTwo = playerTwo.GetComponent<CharController>();

        var instGof = Instantiate(gof);
        instGof.GetComponent<GameOverFade>().doneFade.AddListener(ShowRestartScreen);

        bestDistance = PlayerPrefs.GetFloat("highscore", -1f);
        if(bestDistance > 0)
            Instantiate(flag, new Vector3(0, -1 * (bestDistance - 7.2f + 1.5f), 0), new Quaternion());

    }

    // Update is called once per frame
    void Update()
    {
        if(isPaused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
                MenuManager.instance.Unpause();
            else
                MenuManager.instance.Pause();
        }

        //GAME OVER
        if ((controllerOne.isPlayerDead == true || controllerTwo.isPlayerDead == true) && !gameOver)
        {
            if (curDistance > bestDistance)
                bestDistance = curDistance;

            PlayerPrefs.SetFloat("highscore", bestDistance);

            offender = controllerOne.isPlayerDead ? controllerOne.gameObject : controllerTwo.gameObject;
            paralaxMod = 0;
            gameOver = true;
            playerOne.GetComponent<CharController>().isPlayerDead = true;
            playerTwo.GetComponent<CharController>().isPlayerDead = true;


            onGameOver.Invoke();
            //Time.timeScale = 0;
        }

    }

    private void FixedUpdate()
    {
        if(diffRampDeltaTime < 1 && !gameOver && hasGameStarted)
        {
            paralaxMod = difficultyRamp.Evaluate(diffRampDeltaTime).Remap(0, 1, minSpeed, maxSpeed);

            diffRampDeltaTime += Time.fixedDeltaTime / 30; /// 180;
        }

        if (!gameOver && hasGameStarted)
        {
            curDistance += Time.fixedDeltaTime * paralaxMod * 5;
            UpdateScore();
        }

        //need to know, how close we are to our last best distance. 
    }

    private void ShowRestartScreen()
    {
        MenuManager.instance.retryMenu.SetActive(true);
    }

    public void UpdateScore()
    {
        var score = Mathf.Abs(Mathf.Round((curDistance * 100)) / 100) + "m";
        tmpCurDistance.text = score;
    }
}
