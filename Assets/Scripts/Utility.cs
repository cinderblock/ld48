using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public static class Util
    {
        public static float FrameToNormalizedTime(int maxFrames, int toFrame)
        {
            /*Normalized time expects a float from 0.0f to 1.0f*/
            return (float)toFrame / maxFrames;
        }

        public static float Remap(this float from, float fromMin, float fromMax, float toMin, float toMax)
        {
            return (toMax - toMin) * ((from - fromMin) / (fromMax - fromMin)) + toMin;
        }

        public static float DecibelToLinear(this float dB)
        {
            float linear = Mathf.Pow(10.0f, dB / 20.0f);

            return linear;
        }

        public static float LinearToDecibel(this float linear)
        {
            float dB;

            if (linear != 0)
                dB = 20.0f * Mathf.Log10(linear);
            else
                dB = -144.0f;

            return dB;
        }
    }

    public enum CharType
    {
        PlayerOne,
        PlayerTwo
    }
}
