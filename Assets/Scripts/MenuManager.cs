using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [HideInInspector]
    public GameObject previousMenu;
    [HideInInspector]
    public static MenuManager instance;

    public GameObject mainMenu;
    public GameObject pauseMenu;
    public GameObject optionSubMenu;
    public GameObject retryMenu;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        ResetAll();
    }

    internal static void ResetAll()
    {
        instance.mainMenu.SetActive(false);
        instance.pauseMenu.SetActive(false);
        instance.optionSubMenu.SetActive(false);
        instance.retryMenu.SetActive(false);
    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        optionSubMenu.SetActive(false);
        mainMenu.SetActive(false);
        GameManager.instance.isPaused = true;
    }

    public void Unpause()
    {
        ResetAll();
        GameManager.instance.isPaused = false;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Transition(GameObject go)
    {
        previousMenu = go;
    }

    public void Back()
    {
        previousMenu.SetActive(true);
    }

    public void Retry()
    {
        ResetAll();
        SceneManager.LoadScene(0);
    }

}
