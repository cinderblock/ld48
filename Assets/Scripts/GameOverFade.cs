using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameOverFade : MonoBehaviour
{
    public GameObject AbsoluteCover;
    public GameObject Cover;
    public GameObject Mask;

    SpriteRenderer srCover;
    SpriteRenderer srAbsCover;

    public AnimationCurve fadeCurve;
    float firstFadeDeltaTime = 0;
    float secondFadeDeltaTime = 0;

    bool doneFirstFade;
    bool doneSecondFade;
    bool readyForSecondFade = false;

    bool doFade = false;

    Color coverColor;
    Color absCoverColor;

    private Vector3 posToFade;
    private Vector3 maskOrigScale;

    bool offenderInFront = false;

    public UnityEvent doneFade;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.onGameOver.AddListener(FadeToCharacter);
        srCover = Cover.GetComponent<SpriteRenderer>();
        coverColor = srCover.color;

        srAbsCover = AbsoluteCover.GetComponent<SpriteRenderer>();
        absCoverColor = srAbsCover.color;

        maskOrigScale = Mask.transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (doFade)
        {
            if (!doneFirstFade) {
                //put the offender on top
                if (!offenderInFront)
                    offenderInFront = SetOffenderInFront();
                //fade in the cover.
                coverColor.a = fadeCurve.Evaluate(firstFadeDeltaTime);
                srCover.color = coverColor;
                //lerp the mask position to the correct position
                Mask.transform.position = Vector3.Lerp(Mask.transform.position, posToFade, fadeCurve.Evaluate(firstFadeDeltaTime));
                //start scaling the mask down.
                Mask.transform.localScale = new Vector3(maskOrigScale.x * (1 - fadeCurve.Evaluate(firstFadeDeltaTime)), maskOrigScale.y * (1 - fadeCurve.Evaluate(firstFadeDeltaTime)), maskOrigScale.z);


                firstFadeDeltaTime += Time.fixedDeltaTime / 1f;

                if (firstFadeDeltaTime > 1f)
                    doneFirstFade = true;
            }
            //fade in the absolute cover.
            else if(!doneSecondFade)
            {
                StartCoroutine(IsReadyForSecondFade());
                if (readyForSecondFade)
                {
                    absCoverColor.a = fadeCurve.Evaluate(secondFadeDeltaTime);
                    srAbsCover.color = absCoverColor;

                    secondFadeDeltaTime += Time.fixedDeltaTime / 1f;

                    if (secondFadeDeltaTime > 1f)
                        doneSecondFade = true;
                }
            }
            //tell the game manager that we're done so that it can display a "replay" button.
            else if(doneFirstFade && doneSecondFade)
            {
                doFade = false;
                doneFade.Invoke();
            }
        }
    }

    private IEnumerator IsReadyForSecondFade()
    {
        yield return new WaitForSeconds(1.2f);
        readyForSecondFade = true;
    }

    private void FadeToCharacter()
    {
        posToFade = GameManager.instance.offender.transform.position;
        doFade = true;
    }

    private bool SetOffenderInFront()
    {
        GameManager.instance.offender.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        GameManager.instance.offender.GetComponent<SpriteRenderer>().sortingOrder = 10;
        return true;
    }
}
