using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    [SerializeField]
    public float paralaxStrength = 5;

    public Vector3 newPos;

    // Start is called before the first frame update
    void Start()
    {
        newPos = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        newPos.y = newPos.y + ((paralaxStrength * GameManager.instance.paralaxMod) * Time.deltaTime);
        gameObject.transform.position = new Vector3(transform.position.x, newPos.y, transform.position.z);
    }
}
