using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class CharController : MonoBehaviour
{
    public CharType type;

    int movement = 0;
    float prevVelocity = 0;
    //Vector3 moveVector;
    bool canMoveLeft = true;
    bool canMoveRight = true;
    public bool isPlayerDead = false;
    bool isTouchingWall;
    Rigidbody2D rigidBody;
    SpriteRenderer sr;

    [SerializeField]
    PhysicsMaterial2D defaultBounce;
    [SerializeField]
    PhysicsMaterial2D bigBounce;

    public GameObject leftBarrier;
    public GameObject rightBarrier;

    bool doHurt = false;
    bool iframes = false;

    public Color hurtColor;
    public Color originalColor;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        rigidBody.sharedMaterial = defaultBounce;

        sr = gameObject.GetComponent<SpriteRenderer>();
        //originalColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {
        //character controls
        if (!isPlayerDead)
        {
            if (Input.GetKey(ControlController.getLeftCode(type)))
            {
                StartGame();
                if (canMoveLeft)
                {
                    movement = -1;
                }
                canMoveRight = true;
            }
            else if (Input.GetKey(ControlController.getRightCode(type)))
            {
                StartGame();
                if (canMoveRight)
                {
                    movement = 1;
                }
                canMoveLeft = true;
            }
            else
                movement = 0;
        }
        else
            movement = 0;



        if (prevVelocity != rigidBody.velocity.x)
        {
            if (rigidBody.velocity.x > 0)
                sr.flipX = true;
            else if (rigidBody.velocity.x < 0)
                sr.flipX = false;
            prevVelocity = rigidBody.velocity.x;

        }

        if (movement > 0)
            sr.flipX = true;
        else if (movement < 0)
            sr.flipX = false;

    }


    private void FixedUpdate()
    {
        if (movement < 0)
            rigidBody.AddForce(Vector2.left * 20 * (GameManager.instance.paralaxMod / 2));
        else if (movement > 0)
            rigidBody.AddForce(Vector2.right * 20 * (GameManager.instance.paralaxMod / 2));

        rigidBody.velocity = Vector2.ClampMagnitude(rigidBody.velocity, 10);

        if(doHurt)
        {
            //flash character
            doHurt = false;
            StartCoroutine(DoFlash(5));
        }

        if (isPlayerDead)
            rigidBody.velocity = Vector2.zero;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var tag = other.tag;
        if (tag == "Player")
        {
            //make big bounce if not touching wall
            if (!isTouchingWall)
            {
                rigidBody.sharedMaterial = bigBounce;
            }
        }
        else if(tag == "Barrier")
        {
            //make smol bounce
            rigidBody.sharedMaterial = defaultBounce;
            //make sure to remember that we're touching a wall
            isTouchingWall = true;
        }
        else if(tag == "Obstacle")
        {
            //male smol bounce
            rigidBody.sharedMaterial = defaultBounce;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        isTouchingWall = false;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //we've collided with something.
        var tag = other.gameObject.tag;

        if (tag == "Player")
        {
        }
        else if (tag == "Barrier")
        {
        }
        else if (tag == "Obstacle")
        {
            if (other.collider == other.gameObject.GetComponents<BoxCollider2D>()[0] && !iframes)
            {
                if (GameManager.instance.hearts > 0)
                {
                    GameManager.instance.hearts--;
                    doHurt = true;
                    iframes = true;
                    //do the thing where we flash and stuff to show we got hurt.
                }
                else
                {
                    gameObject.transform.parent = other.gameObject.transform;
                    rigidBody.velocity = Vector2.zero;
                    isPlayerDead = true;
                }
                GameManager.instance.onBadCollission.Invoke();

            }
        }
    }

    //private IEnumerator StopFlash()
    //{
    //    yield return new WaitForSeconds(2f);
    //    doHurt = false;
    //}

    private IEnumerator DoFlash(int numtimes)
    {
        numtimes -= 1;
        sr.color = hurtColor;
        yield return new WaitForSeconds(0.2f);
        sr.color = originalColor;
        yield return new WaitForSeconds(0.2f);
        if (numtimes > 0)
            StartCoroutine(DoFlash(numtimes));
        else if(numtimes == 0)
            iframes = false;
    }

    private void StartGame()
    {
        if(!GameManager.instance.hasGameStarted)
            GameManager.instance.hasGameStarted = true;
    }
}
