using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartContainer : MonoBehaviour
{
    public GameObject[] sprites;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.onBadCollission.AddListener(ChangeSprite);
    }

    public void ChangeSprite()
    {
        foreach (var s in sprites)
            s.SetActive(false);
        if(GameManager.instance.hearts > 0)
            sprites[GameManager.instance.hearts - 1].SetActive(true);
    }
}
