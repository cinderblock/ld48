using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Models;
using System;
using Utilities;

public class AudioManager : MonoBehaviour
{
    /*
     - control ambiance
        - overlay multiple sounds on top of each other
        - choose sounds at random/when to play
     */
    public AudioMixer audioMixer;

    [SerializeField]
    private List<SimpleSound> sounds;

    private float vol;

    private List<SimpleSound> pausedSounds = new List<SimpleSound>();

    public static AudioManager instance;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        foreach (var sound in sounds)
        {
            sound.audioSource = gameObject.AddComponent<AudioSource>();
            sound.audioSource.clip = sound.clip;
            sound.audioSource.outputAudioMixerGroup = AsssignMixerGroup(sound.soundType);
            sound.audioSource.volume = sound.volume; //* vol;
        }

        //audioMixer.GetFloat("MasterVol", out vol);
        //Debug.Log("Master DB Vol: " + vol + " : Linear Vol: " + vol.DecibelToLinear());
        //audioMixer.GetFloat("SFXVol", out vol);
        //Debug.Log("SFX DB Vol   : " + vol + " : Linear Vol: " + vol.DecibelToLinear());
        //audioMixer.GetFloat("MusicVol", out vol);
        //Debug.Log("Music DB Vol : " + vol + " : Linear Vol: " + vol.DecibelToLinear());
        //audioMixer.GetFloat("AmbienceVol", out vol);
        //Debug.Log("Amb DB Vol   : " + vol + " : Linear Vol: " + vol.DecibelToLinear());
    }

    private AudioMixerGroup AsssignMixerGroup(SoundType type)
    {
        string groupName = "";
        if (type == SoundType.Ambiance)
            groupName = "Ambiance";
        else if (type == SoundType.SFX)
            groupName = "SFX";
        else if (type == SoundType.Music)
            groupName = "Music";
        return audioMixer.FindMatchingGroups(groupName)[0];
    }

    public void PlayAmbientSounds()
    {
        var ambianceSounds = sounds.Where(x => x.soundType == SoundType.Ambiance).ToList();

        //play ambient sounds on loop, with respect to volume
        foreach (var sound in ambianceSounds)
        {
            sound.audioSource.loop = true;
            sound.audioSource.Play();
        }
    }

    public void PauseAllSounds()
    {
        foreach (var sound in sounds)
        {
            if (sound.audioSource.isPlaying)
            {
                sound.audioSource.Pause();
                pausedSounds.Add(sound);
            }
        }
    }

    public void UnPauseAllSounds()
    {
        foreach (var sound in pausedSounds)
        {
            sound.audioSource.UnPause();
        }
        pausedSounds.Clear();
    }

    public AudioSource PlaySound(string name, SoundType type = SoundType.None)
    {
        AudioSource source;

        if(type == SoundType.None)
            source = sounds.FirstOrDefault(x => x.name.ToLower() == name.ToLower()).audioSource;
        else
            source = sounds.Where(x => x.soundType == type).FirstOrDefault(x => x.name.ToLower() == name.ToLower()).audioSource;

        source.Play();
        return source;
    }

    public void UpdateMasterVol(float volume)
    {
        volume = volume.LinearToDecibel();
        //audioMixer.SetFloat("MasterVol", volume);
    }

    public void UpdateSFXVol(float volume)
    {
        volume = volume.LinearToDecibel();
        //audioMixer.SetFloat("SFXVol", volume);
    }

    public void UpdateMusicVol(float volume)
    {
        volume = volume.LinearToDecibel();
        //audioMixer.SetFloat("MusicVol", volume);
    }

    public void UpdateAmbienceVol(float volume)
    {
        volume = volume.LinearToDecibel();
        //audioMixer.SetFloat("AmbienceVol", volume);
    }
}
