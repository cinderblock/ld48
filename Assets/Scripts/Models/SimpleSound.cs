﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Models
{
    [Serializable]
    public class SimpleSound
    {
        public string name = "";
        public AudioClip clip;

        public float volume
        {
            get
            {
                return _volume;
            }
            set
            {
                _volume = value;
                audioSource.volume = value;
            }
        }
        [Range(0f, 1f)]
        [SerializeField]
        private float _volume = 1f;

        public SoundType soundType;

        [HideInInspector]
        public AudioSource audioSource;


        [HideInInspector]
        public float defaultVolume
        {
            get
            {
                return _defaultVolume;
            }
            private set
            {
                _defaultVolume = volume;
            }
        }
        private float _defaultVolume = 1f;

    }
}

public enum SoundType
{
    None,
    Ambiance,
    Music,
    SFX
}
