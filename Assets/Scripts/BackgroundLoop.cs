using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLoop : MonoBehaviour
{
    private SpriteRenderer sr;
    private Vector3 newPos;
    private Paralax paralax;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        newPos = gameObject.transform.position;
        paralax = GetComponent<Paralax>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y >= sr.bounds.size.y)
        {
            newPos.y = (gameObject.transform.position.y - (2 * sr.bounds.size.y)) + 0.075f;
            gameObject.transform.position = newPos;
            paralax.newPos = transform.position;
        }
    }
}
