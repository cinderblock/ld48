using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Flag : MonoBehaviour
{
    public TMPro.TextMeshProUGUI tmDistance;
    private void Start()
    {
        //update to correct distance
        tmDistance.text = Mathf.Abs(Mathf.Round((transform.position.y*100))/100) + "m";
    }
}
