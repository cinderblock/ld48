using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    List<GameObject> spawnableObstacles;

    Vector3 newSpawnPos;
    public bool canSpawnObstacle = true;

    // Start is called before the first frame update
    void Start()
    {
        newSpawnPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //while(true) //get "if game end" from game manager
        //{
            if (canSpawnObstacle && GameManager.instance.hasGameStarted)
            {
                StartCoroutine(SpawnObstacle());
            }
        //}
    }

    private IEnumerator SpawnObstacle()
    {
        canSpawnObstacle = false;
        //spawn obstacle from list
        var obsToSpawn = spawnableObstacles[Random.Range(0, spawnableObstacles.Count)];
        obsToSpawn.transform.position = new Vector3(Random.Range(-6.5f, 6.5f),newSpawnPos.y,newSpawnPos.z);
        Instantiate(obsToSpawn);
        float mod = GameManager.instance.paralaxMod > 0 ? GameManager.instance.paralaxMod : GameManager.instance.minSpeed;
        mod *= .9f;
        yield return new WaitForSeconds(Random.Range(2.5f/mod, 4f/mod)); //speed up with game distance. 
        canSpawnObstacle = true;
    }
}
