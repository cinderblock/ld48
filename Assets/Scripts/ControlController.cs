﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utilities;


public static class ControlController
{
    public static KeyCode getLeftCode(CharType type)
    {
        if (type == CharType.PlayerOne)
            return KeyCode.A;
        else
            return KeyCode.LeftArrow;
    }

    public static KeyCode getRightCode(CharType type)
    {
        if (type == CharType.PlayerOne)
            return KeyCode.D;
        else
            return KeyCode.RightArrow;
    }
}